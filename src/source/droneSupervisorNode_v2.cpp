#include <iostream>
#include <string>
#include <vector>


#include "ros/ros.h"

#include "droneSupervisor_v2.h"
#include "dronemoduleinterface.h"

using namespace std;



int main(int argc, char **argv)
{
    ros::init(argc, argv, "Supervisor_Server");
    ros::NodeHandle n;

    DroneSupervisor MyDroneSupervisor;


    DroneModuleInterface* dmi;
    dmi = new DroneModuleInterface(std::string(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR));
    MyDroneSupervisor.addModule(dmi);

    dmi = new DroneModuleInterface(std::string(MODULE_NAME_TRAJECTORY_CONTROLLER));
    MyDroneSupervisor.addModule(dmi);

    dmi = new DroneModuleInterface(std::string(MODULE_NAME_ARUCO_EYE));
    MyDroneSupervisor.addModule(dmi);

    dmi = new DroneModuleInterface(std::string(MODULE_NAME_LOCALIZER));
    MyDroneSupervisor.addModule(dmi);

    dmi = new DroneModuleInterface(std::string(MODULE_NAME_OBSTACLE_PROCESSOR));
    MyDroneSupervisor.addModule(dmi);

    dmi = new DroneModuleInterface(std::string(MODULE_NAME_TRAJECTORY_PLANNER));
    MyDroneSupervisor.addModule(dmi);

    dmi = new DroneModuleInterface(std::string(MODULE_NAME_YAW_PLANNER));
    MyDroneSupervisor.addModule(dmi);

    dmi = new DroneModuleInterface(std::string(MODULE_NAME_MISSION_PLANNER));
    MyDroneSupervisor.addModule(dmi);

    dmi = new DroneModuleInterface(std::string(MODULE_NAME_IBVS_CONTROLLER));
    MyDroneSupervisor.addModule(dmi);

    dmi = new DroneModuleInterface(std::string(MODULE_NAME_TRACKER_EYE));
    MyDroneSupervisor.addModule(dmi);

    dmi = new DroneModuleInterface(std::string(MODUlE_NAME_OPENTLD_TRANSLATOR));
    MyDroneSupervisor.addModule(dmi);




    MyDroneSupervisor.open(n);
    ROS_INFO("Supervisor Service Ready.");
    ros::spin();

    return 0;
}





