#include "droneSupervisor_v2.h"
using namespace std;

DroneSupervisor::DroneSupervisor()
{

}

DroneSupervisor::~DroneSupervisor() 
{
    drone_modules.DestroyContent();
}

bool DroneSupervisor::batteryServCall(std_srvs::Trigger::Request& request, std_srvs::Trigger::Response& response)
{
    response.success = true;
    response.message = "ok";
    return true;
}

bool DroneSupervisor::wifiServCall(std_srvs::Trigger::Request& request, std_srvs::Trigger::Response& response)
{
    response.success = true;
    response.message = "ok";
    return true;
}

bool DroneSupervisor::moduleIsStartedServCall(droneMsgsROS::askForModule::Request& request, droneMsgsROS::askForModule::Response& response)
{

    if(request.module_name == "all")
    {
        response.ack = true;
        if(!drone_modules.allModuleAreStarted())
            response.ack = false;
    }
    else
    {
        response.ack = false;
        if(drone_modules.moduleIsStarted(request.module_name))
            response.ack = true;
    }

    return true;
}

bool DroneSupervisor::moduleIsOnlineServCall(droneMsgsROS::askForModule::Request& request, droneMsgsROS::askForModule::Response& response)
{
    if(request.module_name == "all")
    {
        response.ack = true;
        if(!drone_modules.allModuleAreOnline())
            response.ack = false;
    }
    else
    {
        response.ack = false;
        if(drone_modules.moduleIsOnline(request.module_name))
            response.ack = true;
    }

    return true;
}

void DroneSupervisor::open(ros::NodeHandle & nIn) 
{
    n = nIn;

    DroneModule::open(n);

    drone_modules.Open(n);

    batteryServerSrv = n.advertiseService("batteryIsOk",&DroneSupervisor::batteryServCall,this);
    wifiServerSrv = n.advertiseService("wifiIsOk",&DroneSupervisor::wifiServCall,this);
    moduleIsStartedServerSrv = n.advertiseService("moduleIsStarted",&DroneSupervisor::moduleIsStartedServCall,this);
    moduleIsOnlineServerSrv = n.advertiseService("moduleIsOnline",&DroneSupervisor::moduleIsOnlineServCall,this);

}

bool DroneSupervisor::addModule(DroneModuleInterface *d)
{
    if(drone_modules.Add(d))
        return true;
    else
        return false;
}


