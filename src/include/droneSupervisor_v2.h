#ifndef DRONESUPERVISOR_H
#define DRONESUPERVISOR_H

// ROS
#include "ros/ros.h"

// C++ standar libraries
#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>

#include "droneModuleROS.h"
#include "droneModuleInterfaceList.h"

// Topic and Rosservice names
#include "dronemoduleinterface.h"
#include "droneMsgsROS/droneNavData.h"
#include "droneMsgsROS/dronePose.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/Empty.h"
#include "std_srvs/Trigger.h"
#include "communication_definition.h"
#include <geometry_msgs/Vector3Stamped.h>
#include <droneMsgsROS/droneAltitude.h>
#include <droneMsgsROS/vector2Stamped.h>
#include <droneMsgsROS/battery.h>
#include <droneMsgsROS/droneStatus.h>
#include "droneMsgsROS/droneCommand.h"
#include "droneMsgsROS/askForModule.h"

#define FREQ_ARCHITECTURE_SUPERVISOR 20.0

class DroneSupervisor : public DroneModule
{
private:
    ros::NodeHandle n;
    DroneModuleInterfaceList drone_modules;


    ros::ServiceServer batteryServerSrv;
    ros::ServiceServer wifiServerSrv;
    ros::ServiceServer moduleIsStartedServerSrv;
    ros::ServiceServer moduleIsOnlineServerSrv;



public:
    DroneSupervisor();
    ~DroneSupervisor();
    void open(ros::NodeHandle & nIn);
    bool batteryServCall(std_srvs::Trigger::Request& request, std_srvs::Trigger::Response& response);
    bool wifiServCall(std_srvs::Trigger::Request& request, std_srvs::Trigger::Response& response);
    bool moduleIsStartedServCall(droneMsgsROS::askForModule::Request& request, droneMsgsROS::askForModule::Response& response);
    bool moduleIsOnlineServCall(droneMsgsROS::askForModule::Request& request, droneMsgsROS::askForModule::Response& response);
    bool addModule(DroneModuleInterface *d);
    DroneModuleInterfaceList getDroneModuleInterfaceList(){return drone_modules;}

};

#endif // DRONESUPERVISOR_H
